#!/bin/bash

#echo -n "Please enter which version of php you want to install:-"
#read php


apt-get update
apt-get install -y software-properties-common
add-apt-repository ppa:ondrej/php
apt-get update

echo "#############################"
echo "Installing PHP$php........."
echo "#############################"

sudo apt install php7.4 -y

sudo apt install php-pear -y


echo "Checking If Swoole extension is already there or not"
ext-module=`php -m | grep swoole`
if  [ "$ext-module" == swoole ]; then

	echo "PHP extension swoole already added"
	sleep 5
	exit

else
	sudo apt install php-dev -y
	sudo pecl install swoole

fi
echo "extension=swoole.so" >> /etc/php/7.4/cli/php.ini

new_ext=`php -m | grep swoole`

if  [ "$new_ext" == swoole ]; then

	echo "PHP extension swoolw has been added now"

fi

################################################
####### Mysql Installation #####################
################################################

echo -n "Do you want to install latest version of mysql [y/n/skip]:-"
read mysql


if [ $mysql == "y" ]; then

        sudo apt update
        sudo apt install mysql-server -y
        sudo mysql_secure_installation

        echo -n "Please enter Mysql root password:-" 
        read pass
        mysql -e "SELECT user,authentication_string,plugin,host FROM mysql.user;"
        mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$pass';"
        mysql -u root -p$pass -e "FLUSH PRIVILEGES;"
        mysql -u root -p$pass -e "SELECT user,authentication_string,plugin,host FROM mysql.user;"

fi

if [ $mysql == "n"]; then
        echo -n "Which version of Mysql do you want to install? -"
        read version
        sudo apt update
        sudo apt install mysql-server -y
        sudo mysql_secure_installation
	sudo apt-get install mysql-server-$version mysql-server-core-$version mysql-client-$version mysql-client-core-$version
fi

if [ $mysql == "skip" ]; then
	echo "Mysql installation has been skip"
fi

################################################
####### Git installation and cloning ###########
################################################

echo -n "Do you want to clone your project repo [y/n] :-"
read ans

if [ $ans == "y" ]; then
echo -n "Please Enter Git Repo URL:-"
read repo

echo -n "Please Enter Git Username:-"
read user

echo -n "Please Enter Git Password:-"
read pass

echo -n "Please Enter Git Clone path:-"
read path

	sudo apt-get install git -y
	echo "Git Cloning is started now"
	git clone  https://$user:$pass@$git $path
	echo "Git Cloning is finished..."
	sleep 5
fi

if [ $ans == "n" ]; then
	echo "You have skiped Git clone"
fi
